package com.houngedevelopment.restapitester;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by hounge on 9/30/14.
 */
public class LoginActivity extends Activity {
    public String Stored_Token = "TokenFile";
    EditText UserName;
    EditText Pass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        // Set up the login form.
        UserName = (EditText) findViewById(R.id.UserName);
        Pass = (EditText) findViewById(R.id.Pass);
    }

     public void loginClick(View view) {
        // Validate the log in data
        boolean validationError = false;
        StringBuilder validationErrorMessage =
                new StringBuilder(getResources().getString(R.string.error_intro));
        if (isEmpty(UserName)) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_username));
        }
        if (isEmpty(Pass)) {
            if (validationError) {
                validationErrorMessage.append(getResources().getString(R.string.error_join));
            }
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_password));
        }
        validationErrorMessage.append(getResources().getString(R.string.error_end));

        // If there is a validation error, display the error
        if (validationError) {
            Toast.makeText(LoginActivity.this, validationErrorMessage.toString(), Toast.LENGTH_LONG)
                    .show();
            return;
        }


        final ProgressDialog dlg = new ProgressDialog(LoginActivity.this);
        dlg.setTitle("Please wait.");
        dlg.setMessage("Logging in.  Please wait.");
        dlg.show();


        MessageDigest encoded = null;
        try {
            encoded = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String text = Pass.getText().toString();
        try {
            encoded.update(text.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            Log.v("ewwor", "someting wong");


            e.printStackTrace();
        }
        byte[] passhash = encoded.digest();
        BigInteger bigInt = new BigInteger(1, passhash);
        String hashed = bigInt.toString(16);

        String Username = UserName.getText().toString();
        String token = (Username + ":" + hashed);

        byte[] data = new byte[0];
        try {
            data = token.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String encodedhash = Base64.encodeToString(data, Base64.NO_WRAP);
        final String basicAuth = "Basic " + encodedhash;
        try {
            Resources resources=getResources();
            final String urla=String.format(resources.getString(R.string.web_page_a));
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient client = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(urla);
            httpget.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
            httpget.setHeader("Authorization", basicAuth);
            HttpResponse response = client.execute(httpget);
            Header[] headers = response.getAllHeaders();
            for (Header header : headers) {
                System.out.println("Key : " + header.getName()
                        + " ,Value : " + header.getValue());
            }
            HttpEntity resEntityGet = response.getEntity();
            if (resEntityGet != null) {
                //do something with the response
                String Inkpon_TokenKey = response.getFirstHeader("Inkpon_TokenKey").getValue();
                Log.i("InkPonToken", Inkpon_TokenKey);
                SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
                editor.putString("Token", Inkpon_TokenKey );
                editor.apply();
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }


}


