package com.houngedevelopment.restapitester;



import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.app.Activity;

/**
 * Created by hounge on 9/28/14.
 */

public class MainActivity extends Activity {
    public static final String Stored_Token = "TokenFile";

    EditText rest_api;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //Toast.makeText(getApplicationContext(), storedtoken, Toast.LENGTH_LONG).show();
    }

    // used to call the API you enter
    public void submitclick(View view) {
        rest_api = (EditText) findViewById(R.id.restapi);

        String urla = rest_api.getText().toString();

        //executes the API call
        new HttpAsyncTask().execute(urla);
    }

    public  String POST(String url) {

        String result = "";

        try {
            SharedPreferences prefs = getSharedPreferences(Stored_Token, MODE_PRIVATE);
            String token = prefs.getString("Token", null);

            //Creates JSONObject
            JSONObject parametersList = new JSONObject();
            parametersList.put("Token", token);
            System.out.println(Stored_Token);

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost request = new HttpPost(url);
            request.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
            request.setEntity(new StringEntity(parametersList.toString(), HTTP.UTF_8));

            HttpResponse response = httpClient.execute(request);

            final int statusCode = response.getStatusLine().getStatusCode();
            final String jsonResponse = EntityUtils.toString(response.getEntity());

            if (statusCode != HttpStatus.SC_OK) {
                Log.w("TAG", "Error in web request: " + statusCode);
                Log.w("TAG", jsonResponse);
                return null;
            } else {
                Log.i("TAG", jsonResponse);
                return jsonResponse;
              }
        } catch (IOException e) {
            e.printStackTrace();
          } catch (JSONException e) {
            e.printStackTrace();
            }
        return result;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return POST(urls[0]);
        }

        protected void onPostExecute(String result) {
            //displays returned info in TextView
            TextView response = (TextView) findViewById(R.id.textView);
            response.setText(result);
        }

    }

}